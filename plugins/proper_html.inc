<?php

/**
 * @file
 * Replace wordpress newlines with proper HTML newlines (paragraph tags).
 */

$plugin = array(
  'form' => 'replace_wordpress_newlines_proper_html_form',
  'callback' => 'replace_wordpress_newlines_proper_html_callback',
  'name' => 'Replace Wordpress Newlines',
  'multi' => 'loop',
  'category' => 'HTML',
);

function replace_wordpress_newlines_proper_html_form($importer, $element_key, $settings) {
  $form = array();
  $form['help']['#value'] = t('Replace wordpress newlines with proper HTML newlines (paragraph tags).');

  return $form;
}

function replace_wordpress_newlines_proper_html_callback($source, $item_key, $element_key, &$field, $settings) {
  // Makes markup proper. From core's filter.module.
  $field = _filter_autop($field);
}

